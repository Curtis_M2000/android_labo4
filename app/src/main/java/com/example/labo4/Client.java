package com.example.labo4;

public class Client {
    private int id;
    private String first_name;
    private String last_Name;
    private String adresse;
    private String user;
    private String password;
    private double balance;
    private double credit;

    public Client(int id, String first_name, String last_Name, String adresse, String user, String password, double balance, double credit){
        this.id = id;
        this.first_name = first_name;
        this.last_Name = last_Name;
        this.adresse = adresse;
        this.user = user;
        this.password = password;
        this.balance = balance;
        this.credit = credit;
    }

    public  Client(){

    }

    public int getId() {
        return id;
    }

    public String getLast_Name() {
        return last_Name;
    }

    public double getBalance() {
        return balance;
    }

    public double getCredit() {
        return credit;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getPassword() {
        return password;
    }

    public String getUser() {
        return user;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public void setLast_Name(String last_Name) {
        this.last_Name = last_Name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
