package com.example.labo4;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class InfoPage extends AppCompatActivity {
    public MyDBAdapter dbAdapter;
    private ArrayList<Client> listClient =  new ArrayList<Client>();
    private ArrayList<String> listNom = new ArrayList<String>();
    private ArrayAdapter<String> listAdapter;
    private ListView list1;
    PopupMenu pop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_page);

        dbAdapter = new MyDBAdapter(this);

        list1 = (ListView)findViewById(R.id.listClient);
        listClient = dbAdapter.selectAllClient();

        int i = 0;
        for (Client client : listClient) {
            listNom.add(i+1 + "-  " + client.getFirst_name() + " " + client.getLast_Name());
            i++;
        }

        initArrayAdapter();
        list1.setAdapter(listAdapter);

        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int clientId = listClient.get(position).getId();
                PopUpMenu(view, clientId);
            }
        });
    }

    private void initArrayAdapter() {
        listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listNom);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        super.onOptionsItemSelected(item);
        final Dialog dialPop;

        switch (item.getItemId()){
            case R.id.createAccount:
                dialPop = new Dialog(this);
                dialPop.setContentView(R.layout.createaccount);

                Window window = dialPop.getWindow();
                window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                dialPop.show();

                Button btnCreate = (Button)dialPop.findViewById(R.id.c_createaccount);

                btnCreate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView fname = (TextView)dialPop.findViewById(R.id.c_firstname);
                        TextView lname = (TextView)dialPop.findViewById(R.id.c_lastname);
                        TextView adresse = (TextView)dialPop.findViewById(R.id.c_adresse);
                        TextView user = (TextView)dialPop.findViewById(R.id.c_user);
                        TextView password = (TextView)dialPop.findViewById(R.id.c_password);

                        dbAdapter.createAccount(fname.getText().toString(), lname.getText().toString(), adresse.getText().toString(), user.getText().toString(), password.getText().toString());
                        Intent i = new Intent(getContext(), InfoPage.class);
                        startActivity(i);
                        finish();
                    }
                });
            break;

            case(R.id.stubbornAccounts):

            break;

            case R.id.exit:
                AlertDialog alert = new android.app.AlertDialog.Builder(getContext()).create();
                alert.setCancelable(false);
                alert.setTitle("Exit");
                alert.setMessage("Are you sure you want to exit?");

                alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }
                );

                alert.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                });
                alert.show();
            break;
        }
        return true;
    }

    public void PopUpMenu(View v, int idClient){
        pop = new PopupMenu(this, v, Gravity.RIGHT);
        MenuInflater minf = getMenuInflater();
        minf.inflate(R.menu.popupmenu, pop.getMenu());
        pop.show();

        final int accountId = idClient;

        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final Intent i = new Intent(getContext(), InfoPage.class);
                final Dialog dialPop;
                Window window;

                switch(item.getItemId()){
                    case R.id.deleteAccount:
                        AlertDialog alert = new android.app.AlertDialog.Builder(getContext()).create();
                        alert.setCancelable(false);
                        alert.setTitle("Delete Account");
                        alert.setMessage("Are you sure you want to delete this account?");

                        alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                }
                        );

                        alert.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dbAdapter.deleteAccount(accountId);
                                startActivity(i);
                                finish();
                            }
                        });
                        alert.show();
                    break;

                    case R.id.updateAccount:
                        dialPop = new Dialog(getContext());
                        dialPop.setContentView(R.layout.updateaccount);

                        window = dialPop.getWindow();
                        window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        dialPop.show();

                        Button btnUpdate = (Button)dialPop.findViewById(R.id.u_updateaccount);

                        btnUpdate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TextView user = (TextView)dialPop.findViewById(R.id.u_user);
                                TextView password = (TextView)dialPop.findViewById(R.id.u_password);

                                dbAdapter.updateAccount(accountId, user.getText().toString(), password.getText().toString());
                                startActivity(i);
                                finish();
                            }
                        });
                    break;

                    case R.id.accountInfo:
                        Client client = dbAdapter.account(accountId);

                        dialPop = new Dialog(getContext());
                        dialPop.setContentView(R.layout.accountinfo);

                        window = dialPop.getWindow();
                        window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        dialPop.show();

                        TextView id = (TextView)dialPop.findViewById(R.id.i_id);
                        id.setText(String.valueOf(client.getId()));
                        TextView firstname = (TextView)dialPop.findViewById(R.id.i_firstname);
                        firstname.setText(client.getFirst_name());
                        TextView lastname = (TextView)dialPop.findViewById(R.id.i_lastname);
                        lastname.setText(client.getLast_Name());
                        TextView adresse = (TextView)dialPop.findViewById(R.id.i_adresse);
                        adresse.setText(client.getAdresse());
                        TextView user = (TextView)dialPop.findViewById(R.id.i_user);
                        user.setText(client.getUser());
                        TextView password = (TextView)dialPop.findViewById(R.id.i_password);
                        password.setText(client.getPassword());
                        TextView balance = (TextView)dialPop.findViewById(R.id.i_balance);
                        balance.setText(String.valueOf(client.getBalance()) + "$");
                        TextView credit = (TextView)dialPop.findViewById(R.id.i_credit);
                        credit.setText(String.valueOf(client.getCredit()) + "$");

                        Button btnBack = (Button)dialPop.findViewById(R.id.i_back);

                        btnBack.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialPop.cancel();
                            }
                        });
                    break;

                    case R.id.updateCredit:
                        dialPop = new Dialog(getContext());
                        dialPop.setContentView(R.layout.accountcredit);

                        window = dialPop.getWindow();
                        window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        dialPop.show();

                        TextView oldCredit = (TextView)dialPop.findViewById(R.id.i_oldCredit);
                        oldCredit.setText(String.valueOf(dbAdapter.getCredit(accountId)));

                        Button btnUpdateCredit = (Button)dialPop.findViewById(R.id.btn_updateCredit);

                        btnUpdateCredit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TextView newCredit = (TextView)dialPop.findViewById(R.id.newCredit);

                                dbAdapter.updateAccountCredit(accountId, Double.parseDouble(newCredit.getText().toString()));
                                startActivity(i);
                                finish();
                            }
                        });
                    break;

                    case R.id.updateBalance:
                        dialPop = new Dialog(getContext());
                        dialPop.setContentView(R.layout.accountbalance);

                        window = dialPop.getWindow();
                        window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        dialPop.show();

                        TextView oldBalance = (TextView)dialPop.findViewById(R.id.i_oldBalance);
                        oldBalance.setText(String.valueOf(dbAdapter.getBalance(accountId)));

                        Button btnUpdateBakance = (Button)dialPop.findViewById(R.id.btn_updateBalance);

                        btnUpdateBakance.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                TextView newBalance = (TextView)dialPop.findViewById(R.id.newBalance);

                                dbAdapter.updateAccountBalance(accountId, Double.parseDouble(newBalance.getText().toString()));
                                startActivity(i);
                                finish();
                            }
                        });
                    break;
                }
                return true;
            }
        });
    }

    public Context getContext(){
        return this;
    }
}
