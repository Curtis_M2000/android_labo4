package com.example.labo4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MyDBAdapter {
    private Context context;
    private String dbName = "DBLabo4";
    private int dbVersion = 1;
    private MyDBHelper dbHelper;
    private SQLiteDatabase mySQLiteDatabase;

    public MyDBAdapter(Context context){
        this.context = context;
        dbHelper = new MyDBHelper(context, dbName, null, dbVersion);
    }

    public class MyDBHelper extends SQLiteOpenHelper{
        public MyDBHelper(Context context, String dbName, SQLiteDatabase.CursorFactory factory, int version){
            super(context, dbName, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            String query = "CREATE TABLE clients(id integer primary key autoincrement, first_name text, last_name text, adresse text, user text, password text, balance real, credit real);";
            db.execSQL(query);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            String query = "DROP TABLE IF EXISTS clients;";
            db.execSQL(query);
            onCreate(db);
        }
    }

    public void open(){
        try{
            this.mySQLiteDatabase = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createAccount(String fname, String lname, String adresse, String user, String password){
        open();
        ContentValues cv = new ContentValues();
        cv.put("first_name", fname);
        cv.put("last_name", lname);
        cv.put("adresse", adresse);
        cv.put("user", user);
        cv.put("password", password);
        cv.put("balance", 100.0);
        cv.put("credit",500.0);
        this.mySQLiteDatabase.insert("clients", null, cv);
    }

    public ArrayList<Client> selectAllClient(){
        open();
        ArrayList<Client> tab = new ArrayList<Client>();
        Cursor cursor = mySQLiteDatabase.query("clients", null, null, null, null, null, null);

        if(cursor != null && cursor.moveToFirst()){
            do{
                Client client = new Client(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getDouble(6), cursor.getDouble(7));
                tab.add(client);
            }
            while(cursor.moveToNext());
        }
        return tab;
    }

    public void deleteAccount(int id){
        open();
        this.mySQLiteDatabase.delete("clients", "id=" + id, null);
    }

    public void updateAccount(int id, String user, String password){
        open();
        ContentValues cv = new ContentValues();
        cv.put("user", user);
        cv.put("password", password);
        this.mySQLiteDatabase.update("clients", cv, "id=" + id, null);
    }

    public Client account(int id){
        open();
        Cursor cursor = mySQLiteDatabase.rawQuery("SELECT * FROM clients WHERE id=" + id, null);
        Client client = new Client();

        if(cursor != null && cursor.moveToFirst()){
            do{
                client = new Client(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getDouble(6), cursor.getDouble(7));
            }
            while(cursor.moveToNext());
        }

        return client;
    }

    public double getBalance(int id){
        open();
        double balance = 0.0;
        Cursor cursor = mySQLiteDatabase.rawQuery("SELECT balance FROM clients WHERE id=" + id, null);
        Client client = new Client();

        if(cursor != null && cursor.moveToFirst()){
            do{
                balance = cursor.getDouble(0);
            }
            while(cursor.moveToNext());
        }
        return balance;
    }

    public double getCredit(int id){
        open();
        double credit = 0.0;
        Cursor cursor = mySQLiteDatabase.rawQuery("SELECT credit FROM clients WHERE id=" + id, null);
        Client client = new Client();

        if(cursor != null && cursor.moveToFirst()){
            do{
                credit = cursor.getDouble(0);
            }
            while(cursor.moveToNext());
        }
        return credit;
    }

    public void updateAccountCredit(int id, double newCredit){
        open();
        ContentValues cv = new ContentValues();
        cv.put("credit", newCredit);
        this.mySQLiteDatabase.update("clients", cv, "id=" + id, null);
    }

    public void updateAccountBalance(int id, double newBalance){
        open();
        ContentValues cv = new ContentValues();
        cv.put("balance", newBalance);
        this.mySQLiteDatabase.update("clients", cv, "id=" + id, null);
    }
}
